from peewee import *
from flask_login import UserMixin

database = SqliteDatabase("users.sqlite3")

class BaseModel(Model):
    class Meta:
        database = database

class Users(BaseModel, UserMixin):
    id=AutoField(primary_key=True)
    login = CharField(unique=True)
    password = CharField()

class Flows(BaseModel):
    idUser = IntegerField()
    link = CharField()

def create_tables():
    with database:
        database.create_tables([Users, Flows])
        print("Create tables")

def drop_tables():
    with database:
        database.drop_tables([Users, Flows])
        print("Drop tables")



