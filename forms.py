from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import PasswordInput
from feedparser import *

class Register(FlaskForm):
    login = StringField('Login', validators=[DataRequired(), Length(min=3, max=20)])
    password = StringField('Mot de passe', widget=PasswordInput(hide_value=True))
    pass

class Connection(FlaskForm):
    login = StringField('Login', validators=[DataRequired(), Length(min=3, max=20)])
    password = StringField('Mot de passe', widget=PasswordInput(hide_value=True))
    pass

class AddFlux(FlaskForm):
    link = StringField('Lien du flux', validators=[DataRequired()])
    pass
