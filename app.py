from flask import *
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from forms import *
import requests
from models import *
from wtforms import *
import hashlib
import feedparser

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'wololo'

login_manager = LoginManager() 
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return Users.get(user_id)


@app.route('/')
def index():
    return render_template("index.html")

@app.route('/login', methods=('GET', 'POST' ))
def login_page():
    form = Connection() 
    if form.validate_on_submit():
        ConnectLogin = form.login.data.upper()
        ConnectPasswd = hashlib.sha1(form.password.data.encode()).hexdigest()
        print(ConnectPasswd)
        user = Users.select().where((Users.login == ConnectLogin) & (Users.password == ConnectPasswd)).first()
        print(user)
        if (user == None):
            flash("login ou mot de passe incorrecte")
        else:
            login_user(user)
            current_user.idUser = user.id
            return redirect(url_for('index_flow'))
    return render_template('login.html', form=form)

@app.route('/disconnect')
@login_required
def deconnexion():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=('GET', 'POST' ))
def register():
    form = Register()
    if form.validate_on_submit():
        login = form.login.data
        Passwd = form.password.data
        ConnectLogin = hashlib.sha1(Passwd.encode()).hexdigest()
        user = Users.select().where(Users.login == login).first()
        if(user == None):
            user = Users()
            user.login = login.upper()
            user.password = hashlib.sha1(Passwd.encode()).hexdigest()
            user.save()
            return redirect(url_for('index'))
        else:
            flash("L'utilisateur existe déjà")
            return redirect(url_for('register'))
    return render_template('register.html', form=form)

@app.route('/index_flow', methods=('GET','POST'))
@login_required
def index_flow():
    FlowParse = []
    FlowList = Flows.select().where(Flows.idUser == current_user.id)
    for item in FlowList :
        fluxparse = feedparser.parse(item.link) 
        FlowParse.append(fluxparse)
    return render_template('index_flow.html',FlowParse=FlowParse,Flows=FlowList,zip=zip(FlowParse,FlowList))

@app.route('/flow', methods=['GET', 'POST'])
@login_required
def flow():
    linkFlow = request.args.get('link')
    flowparse = feedparser.parse(linkFlow)
    element = feedparser.parse(linkFlow).entries
    return render_template('flow.html',flowparse=flowparse, element=element)

@app.route('/add', methods=['GET', 'POST'])
@login_required
def add():
    form = AddFlux()
    if form.validate_on_submit():
        flux = Flows.select().where((Flows.link == form.link.data) & (Flows.idUser == current_user.id)).first()
        if(flux == None):
            flow = Flows()
            flow.idUser = current_user.id
            flow.link = form.link.data
            flow.save()
            flash('Flux ajouté')
            return redirect(url_for('index_flow'))   
        else:
            return redirect(url_for('add'))
    return render_template('add.html',form=form)

@app.route('/remove', methods=['GET','POST'])
@login_required
def remove():
    idUser = request.args.get('idUser')
    link = request.args.get('link')
    command = Flows.delete().where((Flows.idUser == idUser) & (Flows.link == link))
    command.execute()
    return redirect(url_for('index_flow'))

@app.cli.command()
def initdb():
    create_tables()

@app.cli.command()
def dropdb():
    drop_tables()
    

